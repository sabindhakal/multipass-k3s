#!/bin/bash
echo "[DEPLOY K3S_MASTER] Create lxc container"
NODES=$(echo master)
for NODE in ${NODES}; do
lxc launch ubuntu:20.04 k3s-${NODE}; done

sleep 10
K3S_MASTER_IP=$(lxc list k3s-master -c 4 | awk '!/IPV4/{ if ( $2 != "" ) print $2}')
echo "MASTER_IP:${K3S_MASTER_IP}"
lxc exec k3s-master -- bash -c 'sudo apt-get update && sudo apt-get upgrade -y && sudo apt-get install -y apt-transport-https'
lxc exec k3s-master -- bash -c "curl -sfL https://get.k3s.io | INSTALL_K3S_EXEC=\"--write-kubeconfig-mode 664 --bind-address ${K3S_MASTER_IP} --advertise-address ${K3S_MASTER_IP} --disable traefik\" sh -"
lxc exec k3s-master -- bash -c 'sudo cat /var/lib/rancher/k3s/server/token' > master_token.yaml
lxc exec k3s-master -- bash -c 'sudo sudo ufw disable' > master_token.yaml

