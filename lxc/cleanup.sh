#!/bin/bash
lxc list
lxc stop k3s-worker1 k3s-worker2
lxc delete k3s-worker1 k3s-worker2 --force
lxc stop k3s-master
lxc delete k3s-master --force
lxc list
