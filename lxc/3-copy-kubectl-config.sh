#!/bin/bash
echo "[COPY kubectl config to host]"
mkdir -p $HOME/.kube
lxc  exec k3s-master -- bash -c "cat /etc/rancher/k3s/k3s.yaml" > $HOME/.kube/config
