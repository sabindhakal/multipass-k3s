#!/bin/bash
echo "[DEPLOY K3S_WORKERS] Create lxc container"
NODES=$(echo worker{1..2})

for NODE in ${NODES}; do
lxc launch ubuntu:20.04 k3s-${NODE}; done

sleep 10
NODE_TOKEN=`cat master_token.yaml`
K3S_MASTER_IP=$(lxc list k3s-master -c 4 | awk '!/IPV4/{ if ( $2 != "" ) print $2}')
for NODE in ${NODES}; do
lxc exec k3s-${NODE}  -- bash -c 'sudo apt-get update && sudo apt-get upgrade -y && sudo apt-get install -y apt-transport-https'
lxc exec k3s-${NODE}  -- bash -c "curl -sfL https://get.k3s.io | K3S_TOKEN=${NODE_TOKEN} K3S_URL=https://${K3S_MASTER_IP}:6443 sh -"
lxc exec k3s-${NODE}  -- bash -c 'sudo sudo ufw disable' > master_token.yaml
done
