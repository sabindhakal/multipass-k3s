#!/bin/bash
NODES=$(echo worker{1..2})

for NODE in ${NODES}; do
multipass launch --name k3s-${NODE} --cpus 2 --mem 2G --disk 8G; done
NODE_TOKEN=`cat master_token.yaml`
# K3S_VERSION=v1.19.7+k3s1

K3S_MASTER_IP=$(multipass info k3s-master | grep IPv4 | awk '{print $2}')
for NODE in ${NODES}; do
multipass exec k3s-${NODE} -- bash -c 'sudo apt-get update && sudo apt-get upgrade -y && sudo apt-get install -y apt-transport-https'
multipass exec k3s-${NODE} -- bash -c "curl -sfL https://get.k3s.io | K3S_TOKEN=${NODE_TOKEN} K3S_URL=https://${K3S_MASTER_IP}:6443 sh -"
done

sleep 30