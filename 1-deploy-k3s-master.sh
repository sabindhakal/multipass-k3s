#!/bin/bash
multipass launch ubuntu --name k3s-master --cpus 2 --mem 2G --disk 8G
# K3S_VERSION=v1.19.7+k3s1
K3S_MASTER_IP=$(multipass info k3s-master | grep IPv4 | awk '{print $2}')
multipass exec k3s-master -- bash -c 'sudo apt-get update && sudo apt-get upgrade -y && sudo apt-get install -y apt-transport-https'
multipass exec k3s-master -- bash -c "curl -sfL https://get.k3s.io | INSTALL_K3S_EXEC=\"--write-kubeconfig-mode 664 --bind-address ${K3S_MASTER_IP} --advertise-address ${K3S_MASTER_IP} --disable traefik\" sh -"
multipass exec k3s-master -- bash -c 'sudo cat /var/lib/rancher/k3s/server/token' > master_token.yaml