Collection of scripts to automate multi-node k3s cluster installation

## About Multipass

https://multipass.run/

## Prerequsists

You need kubectl and multipass installed on your laptop.

### Install multipass (on MacOS Catalina or Linux)

Get the latest Multipass here:

https://github.com/CanonicalLtd/multipass/releases

### Start cluster

```bash
./start.sh
```
NOTE: Check IP address of your nodes 

```
multipass list
```

Adjust the IP addresses in 2-deploy-k3s-nodes.sh & 1-deploy-k3s-master.sh accordingly
## Cleanup

```bash
./cleanup.sh
```