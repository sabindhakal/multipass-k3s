# #!/bin/bash
multipass launch ubuntu --name rancher --cpus 2 --mem 1G --disk 8G

RANCHER_IP=$(multipass info rancher | grep IPv4 | awk '{print $2}')
multipass exec rancher -- bash -c 'sudo apt-get update && sudo apt-get upgrade -y && sudo apt-get install -y apt-transport-https curl'
multipass exec rancher -- bash -c "curl -sSfL https://get.docker.com | sh -"
multipass exec rancher -- bash -c "sudo docker run -d --name rancher --privileged --restart=unless-stopped -p 80:80 -p 443:443 -v /opt/rancher:/var/lib/rancher rancher/rancher:latest"


# multipass exec rancher -- bash -c "helm repo add rancher-latest https://releases.rancher.com/server-charts/latest"

# kubectl apply -f https://raw.githubusercontent.com/jetstack/cert-manager/release-0.9/deploy/manifests/00-crds.yaml
# kubectl create namespace cert-manager
# kubectl label namespace cert-manager certmanager.k8s.io/disable-validation=true
# helm repo add jetstack https://charts.jetstack.io
# helm repo update
# helm install \
#   --name cert-manager \
#   --namespace cert-manager \
#   --version v0.14.2 \
#   jetstack/cert-manager
# kubectl get pods --namespace cert-manager

# helm repo update
# helm install rancher rancher-latest/rancher \
#   --namespace cattle-system \
#   --set hostname=rancher.my.org

# multipass exec k3s-master -- bash -c "curl -sfL https://get.k3s.io | INSTALL_K3S_EXEC=\"--write-kubeconfig-mode 664 --bind-address ${K3S_MASTER_IP} --advertise-address ${K3S_MASTER_IP} --disable traefik\" sh -"
# multipass exec k3s-master -- bash -c 'sudo cat /var/lib/rancher/k3s/server/token' > master_token.yaml

sudo ssh \
      -i /var/root/Library/Application\ Support/multipassd/ssh-keys/id_rsa \
      -L 9000:localhost:80 \
      multipass@192.168.64.10