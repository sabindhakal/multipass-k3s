# #!/bin/bash
sudo apt-get update && sudo apt-get upgrade -y && sudo apt-get install -y apt-transport-https curl
sudo apt-get update && sudo apt-get upgrade -y && sudo apt-get install -y apt-transport-https curl
curl -sSfL https://get.docker.com | sh -
sudo docker run -d --name rancher --privileged --restart=unless-stopped -p 80:80 -p 443:443 -v /opt/rancher:/var/lib/rancher rancher/rancher:latest

