mkdir -p $HOME/.kube
multipass exec k3s-master -- bash -c "cat /etc/rancher/k3s/k3s.yaml" > $HOME/.kube/config
chown -R $(id -u):$(id -g) $HOME/.kube/config